@extension('Layouts.app')
@section('content')

<h1 class="text-center py-5">All Users</h1>

<div class="col-lg-10 offset-lg-1">
	<table class="table table-stripped">
		<thead>
			<tr>
				<th>Name</th>
				<th>Email</th>
				<th>Role</th>
				<th>Action</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>{{$user->name}}</td>
				<td>{{$user->email}}</td>
				<td>{{$user->role_id->name}}</td>
				<td>
					@if($user->role_id == 2)
						<a href="/changeuser{{$user->id}}" class="btn btn-success">Promote</a>
					@else
						<a href="/changeuser{{$user->id}}" class="btn btn-warning">Demote</a>
					@endif
					
					@if(in_array($user->id, $usersWithOrders) || $user->id==1)
					@else
						<form action="/deleteuser/{{$user->id}}" method="POST">
							@csrf
							@method('DELETE')
							<button class="btn btn-danger" type="submit">Delete</button>
						</form>
					@endif
				</td>
			</tr>
		</tbody>
	</table>
</div>

@endsection