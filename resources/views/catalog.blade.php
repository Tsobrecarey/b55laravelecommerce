@extends("layouts.app")
@section("content")

<h1 class="text-center py-3">Catalog</h1>
<div class="d-flex justify-content-end">
	<div col-lg-5>
		<form action="" method="" class="p-3">
			@csrf
			<div class="input-group">
				{{-- eventlisten on input setTimeOut() --}}
				<input type="text" name="search" class="form-control" placeholder="Search items by name or description">
				<div class="input-group-append">
					<button class="btn btn-info" type="submit">Search</button>
				</div>
			</div>
		</form>
	</div>
</div>


@if(Session::has("message"))
	<h4>{{ Session::get('message') }}</h4>
@endif


{{-- Sort --}}
<div class="container">
	<div class="row">
		<div class="col-lg-2">
			<h4>Filter by Category</h4>
			<ul class="list-group">
				@foreach($categories as $category)
				<li class="list-group-item">
					<a href="/catalog/{{$category->id}}">{{$category->name}}</a>
				</li>
				@endforeach
				<li class="list-group-item">
					<a href="">All</a>
				</li>
			</ul>
			<hr>
			<h4>Sort by Price</h4>
			<ul class="list-group">
				<li class="list-group-item">
					<a href="/catalog/sort/asc">Cheapest First</a>
				</li>
				<li class="list-group-item">
					<a href="/catalog/sort/desc">Most Expensive First</a>
				</li>
			</ul>
		</div>
		<div class="col-lg-10">
			<div class="container">
				<div class="row">
					@foreach($items as $indiv_item)
						<div class="col-lg-3 my-2">
							<div class="card">
								<img class="card-img-top" src="{{ asset($indiv_item->imgpath) }}" alt="Screenshot" height="300px">
								<div class="card-body">
									<h4 class="card-title">{{ $indiv_item->name }}</h4>
									<p class="card-body">{{ $indiv_item->price }}</p>
									<p class="card-body">{{ $indiv_item->description }}</p>
								</div>
								<div class="card-footer d-flex justify-content-center align-items-center">
									<form action="/deleteitem/{{ $indiv_item->id }}" method="POST">
										@csrf
										@method('DELETE')
										<button class="btn btn-danger mx-1" type="submit">Delete</button>
									</form>
									<form action="/edititem{{-- /{{ $indiv_item->id }} --}}" method="POST">
										@csrf
										@method('PATCH')
										<button class="btn btn-warning mx-1" type="submit">Edit</button>
									</form>
								</div>
								{{-- <input id="qunatity_{{$indiv_item->id}} --}}
								{{-- <button onclick="addToCart({{`$indiv_item->id`}})" --}}
							</div>
						</div>
					@endforeach
				</div>
			</div>
			{{-- <script type="text/javascript">
				const addToCart=id=>{
				let quantity=document.querySelector('#quantity_'+id).value;
				alert(qunatity+" of item " +id+ " has been added to cart");

				let data=new FormData;

				data.append("_token", "{{ csrf_token() }}");
				data.append("quantity", quantity);

				fetch("/addtocart/"+id, {
					method: "POST",
					body: data
				}).then(res=>res.text())
				.then(res=>console.log(res))
				}

			</script> --}}
		</div>
	</div>
</div>


@endsection