@extends("layouts.app")
@section("content")

<h1 class="text-center py-5">Add Items</h1>

<div col-lg-8>
	<form action="/additem" method="POST" class="form-group mx-5" enctype="multipart/form-data">
		@csrf
		<div class="form-group">
			<label for="name">Item Name</label>
			<input type="text" name="name" class="form-control">
		</div>
		<div class="form-group">
			<label for="text">Description</label>
			<input type="text" name="description" class="form-control">
		</div>
		<div class="form-group">
			<label for="price">Price</label>
			<input type="number" name="price" class="form-control">
		</div>
		<div class="form-group">
			<label for="image">Image</label>
			<input type="file" name="image" class="form-control">
		</div>
		<div class="form-group">
			<label for="category">Category</label>
			<div class="form-group">
				<select name="category" class="form-control">
					@foreach($categories as $indiv_category)
						<option value="{{ $indiv_category->id }}">{{ $indiv_category->name }}</option>
					@endforeach
				</select>
			</div>
		</div>
		<button class="btn btn-info" type="submit">Add Item</button>
	</form>
</div>

@endsection