<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use \App\Order;
use \App\Item;
use Session;

class OrderController extends Controller
{
    public function checkout(){
    	if(Auth::user()){
    		$order=new Order;
    		$order->user_id=Auth::user()->id;
    		$order->status_id=1;
    		$order->payment_id=1;
    		$order->total=0;
    		$order->save();

    		$cart=Session::get('cart');

    		$total=0;

    		foreach ($cart as $itemId => $quantity) {
    			$order->items()->attach($itemId, ["quantity"=>$quantity]);

    			$item=Item::find($itemId);
    			$total+=$item->price*$quantity;
    		}

    		$order->total=$total;
    		$order->save();

    		Session::forget('cart');
    		Session::flash('message', "Order Successfully Placed");
    		return redirect('/catalog');
    	}else{
    		return redirect('/login');
    	}
    }

    public function showOrders(){
    	$order=Order::where('user_id', Auth::user()->id)->get();

    	return view('userviews.orders', compact('orders'));
    }

    public function allOrders(){
        $orders=Order::all();

        return view('adminviews.allorders', compact('orders'));
    }

    public function cancelOrderByAdmin($id){
        $order=Order::find($id);
        $order->status_id=4;
        $order->save();

        return redirect('/allOrders');
    }

    public function cancelOrderByUser($id){
        $order=Order::find($id);
        $order->status_id=3;
        $order->save();

        return redirect('/showOrders');
    }
}
