<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Item;
use \App\Category;
use Session;

class ItemController extends Controller
{
    public function index(){
    	$items=Item::all();
        $categories=Category::all();
    	return view('catalog', compact('items', 'categories'));
    }

    public function search(Request $req){
        $items=Item::where('name', 'LIKE', '%'.$req->search.'%')->orWhere('description', 'LIKE', '%'.$req->search.'%')->get();
        $categories=Category::all();

        if(count($items)==0){
            Session::flash('message', 'No items found');
        }

        return view('catalog', compact('items', 'categories'));
    }

    public function filter($id){
        $items=Item::where('category_id, $id')->get();
        $categories=Category::all();

        return view('catalog', compact('items', 'categories'));
    }

    public function sort($sort){
        $items=Item::orderBy('price', $sort)->get();
        $categories=Category::all();

        return view('catalog', compact('items', 'categories'));
    }

    public function additem(){
    	$categories=Category::all();
    	return view('additem', compact('categories'));
    }

    public function additemstore(Request $reqst){
        // $rules=array(
        // 	"name"=>"required",
        // 	"description"=>"required",
        // 	"price"=>"required|numeric",
        // 	"category_id"=>"required",
        // 	"imgpath"=>"image|mimes:jpeg, jpg, png, gif, tiff, tif, bitmap, webP"
        // );

        // $this->validate($reqst, $rules);

        $new_Item=new Item;
        $new_Item->name=$reqst->name;
        $new_Item->description=$reqst->description;
        $new_Item->price=$reqst->price;
        $new_Item->category_id=$reqst->category;

        $image=$reqst->file('image');
		$image_name= time().".".$image->getClientOriginalExtension();
		$destination="images/";
		$image->move($destination, $image_name);
		$new_Item->imgpath=$destination.$image_name;
		$new_Item->save();

		Session::flash("message", "$newItem->name has been added");
		return redirect('catalog');

    }

    public function destroy($id){
    	$itemtodel=Item::find($id);

    	$itemtodel -> delete();
    	return redirect('/catalog');
    }

        public function edititem(){
        $categories=Category::all();
        return view('edititem', compact('categories'));
    }

    public function showcart(){
    	$items=[];
    	$total=0;

    	if(Session::has('cart')){
    		$cart=Session::get('cart');
    		foreach ($cart as $itemid => $quantity) {
    			$item=Item::find($itemId);
    			$item->quantity=$quantity;
    			$item->subtotal=$item->price * $quantity;
    			$items[]=$item;
    			$total+=$item->subtotal;
    		}
    	}
    	return view('userviews.cart', compact('items', 'total'));
    }

    public function removeItem($id){
        // $cart=Session::get('cart');
        Session::forget("cart.$id");
        return redirect()->back();
    }

   //      $this->validate($reqst, $rules);

   //      $item->name=$reqst->name;
   //      $item->description=$reqst->description;
   //      $item->price=$reqst->price;
   //      $item->category_id=$reqst->category_id;

   //      if($reqst->file('imgpath')!=null){
	  //       $image=$reqst->file('imgpath');
			// $image_name= time().".".$image->getClientOriginalExtension();
			// $destination="images/";
			// $image->move($destination, $image_name);
			// $newItem->imgpath=$destination.$image_name;
   //      }
   //      $newItem->save();
   //      Session::flash('message', "$item->name has been updated");
   //      return redirect('/catalog');

}
