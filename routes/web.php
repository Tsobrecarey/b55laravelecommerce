<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/catalog', 'ItemController@index');

Route::get('/catalog/{id}', 'ItemController@filter');

Route::get('/catalog/sort/{sort}', 'ItemController@sort');

Route::get('/additem', 'ItemController@additem');

Route::post('/additem', 'ItemController@additemstore');

Route::get('/edititem', 'ItemController@edititem');

Route::delete('/deleteitem/{id}', 'ItemController@destroy');

Route::get('/showcart', 'ItemController@showcart');

Route::delete('/removeitem/{id}', 'ItemController@removeItem');

Route::get('/checkout', 'OrderController@checkout');

Route::get('/showorders', 'OrderController@showOrders');

Route::get('/allorders', 'OrderController@allOrders');

Route::get('/cancelorder/{id}', 'OrderController@cancelOrderByAdmin');

Route::get('/cancelorder/{id}', 'OrderController@cancelOrderByUser');

Route::get('/allusers', 'UserController@index');

Route::get('/changeuser/{id}', 'UserController@changeRole');

Route::delete('/deleteuser/{id}', 'UserController@destroy');

Route::post('/search', 'ItemController@search');